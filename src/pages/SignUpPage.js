import React from "react";
import SignUpForm from "../components/SignUpPage/SignUpForm";
import styled from "styled-components";

export default function SignUpPage() {
  return (
    <SignUpPageWrapper>
      <SignUpForm />
    </SignUpPageWrapper>
  );
}

const SignUpPageWrapper = styled.div`
  p {
    text-align: center;
    margin-top: 1rem;
  }

  a {
    color: black;
    text-decoration: underline;
  }

  .form-control,
  .form-control:focus {
    background: rgba(0, 0, 0, 0.1);
    border: none !important;
    outline: none;
    border-color: #ff0000;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 2px rgba(0, 0, 0, 0.6);
  }
`;
