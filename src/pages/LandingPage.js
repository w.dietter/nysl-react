import React from 'react';
import styled from 'styled-components';
import img from '../imgs/soccer-line.jpg';

const LandingPage = () => {
	return (
		<BackgroundImg>
			<LandingPageWrapper>
				<h1>Nysl Mobile App</h1>
			</LandingPageWrapper>
		</BackgroundImg>
	);
};
export default LandingPage;

const LandingPageWrapper = styled.main`
	height: 100%;
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	background: rgba(0, 0, 0, 0.2);
	color: white;
	text-align: center;

	h1 {
		font-size: 4rem;
		margin-bottom: 0.75rem;
		line-height: 1.1;
	}
`;

const BackgroundImg = styled.div`
	height: calc(var(--main-title) + var(--main-height));
	background: #333 url(${img}) no-repeat center center/cover;
`;
