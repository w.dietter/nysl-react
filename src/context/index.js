import { GamesContext, GamesContextConsumer } from './games/gamesContext';
import GamesContextProvider from './games/gamesContext';
import PostContextProvider from './posts/postContext';
import { PostContext, PostContextConsumer } from './posts/postContext';

export {
	GamesContext,
	GamesContextProvider,
	GamesContextConsumer,
	PostContext,
	PostContextProvider,
	PostContextConsumer
};
