import React from 'react';
import styled from 'styled-components';
import { FaUserAlt, FaTrashAlt } from 'react-icons/fa';
import withAuthentication from '../../../session/withAuthentication';

/* components */
import CommentsContainer from '../comments/CommentsContainer';
import { compose } from 'recompose';
import withPostContext from '../../../hoc/withPostContext/withPostContext';
import withToast from '../../../hoc/withToast/withToast';

const PostItem = ({
	post: { user, title, body, date, id },
	authUser,
	postContext: { deleteGamePost },
	toastContext: { showToast }
}) => {
	const handleDeletePost = () => {
		deleteGamePost(id);
		showToast('Post Deleted!', 3000);
	};
	return (
		<PostItemWrapper className="shadow">
			<div className="post-header">
				<div className="user">
					<FaUserAlt className="icon" />
					<span>{user.displayName}</span>
				</div>
				<h1 className="title">{title}</h1>
				<div className="delete">
					{authUser &&
						(authUser.uid === user.uid ? (
							<FaTrashAlt onClick={handleDeletePost} />
						) : (
							<div />
						))}
				</div>
			</div>
			<p>{body}</p>
			<p className="date">{new Date(date).toLocaleString()}</p>
			<CommentsContainer postId={id} />
		</PostItemWrapper>
	);
};

export default compose(
	withToast,
	withAuthentication,
	withPostContext
)(PostItem);

const PostItemWrapper = styled.li`
	width: 90%;
	margin: 1rem auto;
	overflow: scroll;
	background: rgba(200, 210, 200, 0.8);
	border-radius: 5px;

	.post-header {
		display: flex;
		justify-content: center;
		align-items: center;
		background: black;
		color: white;
		padding: 0.25rem 1rem;
		font-size: 0.8rem;
	}

	.post-header .user {
		width: 30%;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.post-header .title {
		width: 50%;
		text-align: center;
	}

	.post-header .delete {
		width: 20%;
		text-align: right;
	}

	.icon {
		margin-right: 1rem;
	}

	.date {
		font-size: 0.75rem;
		padding: 0.25rem;
		margin: 0;
		text-align: right;
	}

	p {
		padding: 1rem;
		max-width: 100%;
		word-wrap: break-word;
	}

	small {
		font-size: 0.8rem;
	}
`;
