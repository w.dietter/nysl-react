import React from 'react';
import styled from 'styled-components';

const Title = ({ title }) => {
	return (
		<TitleWrapper>
			<h1>{title}</h1>
		</TitleWrapper>
	);
};

export default Title;

const TitleWrapper = styled.div`
	width: 100%;
	height: var(--main-title);
	display: flex;
	align-items: center;
	justify-content: center;
	h1 {
		font-size: 1.5rem;
		font-weight: bold;
	}

	@media screen and (orientation: landscape) {
		height: calc(var(--main-title) + 0.5rem);
	}
`;
