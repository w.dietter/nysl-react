import React from "react";

const FirebaseContext = React.createContext(null);

export const withFirebaseContext = (WrappedComponent) => {
  return (props) => {
    return (
      <FirebaseContext.Consumer>
        {(firebase) => {
          return <WrappedComponent {...props} firebase={firebase} />;
        }}
      </FirebaseContext.Consumer>
    );
  };
};

export default FirebaseContext;
