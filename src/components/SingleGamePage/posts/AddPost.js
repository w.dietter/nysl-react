import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

/* hoc */
import withPostContext from '../../../hoc/withPostContext/withPostContext';
import { compose } from 'recompose';
import withToast from '../../../hoc/withToast/withToast';

class AddPost extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			body: '',
			error: null
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		console.log(this.props);
	}

	static propTypes = {
		toggleAddPost: PropTypes.func.isRequired,
		game: PropTypes.object.isRequired,
		authUser: PropTypes.object.isRequired
	};

	handleChange({ target: { value, name } }) {
		this.setState({ [name]: value });
	}

	async handleSubmit(e) {
		e.preventDefault();
		/* extract current User and game info to create the post */
		const {
			authUser: { displayName, email, uid },
			game: { id },
			toggleAddPost,
			toastContext: { showToast }
		} = this.props;
		const { addGamePost } = this.props.postContext;

		try {
			const res = await addGamePost({
				...this.state,
				user: {
					displayName,
					email,
					uid
				},
				gameId: id
			});
			console.log(res);
			this.setState({ title: '', body: '', error: null });
			showToast('Post created', 3000);
			toggleAddPost();
		} catch (error) {
			this.setState({ error });
		}
	}

	render() {
		return (
			<AddPostWrapper>
				<div className="container mt-4">
					<div className="row">
						<form onSubmit={this.handleSubmit} className="col-10 mx-auto mt-4">
							<div className="form-group text-light">
								<label htmlFor="title">Title</label>
								<input
									className="form-control bg-light"
									type="text"
									name="title"
									value={this.state.title}
									onChange={this.handleChange}
									autoComplete="off"
								/>
							</div>
							<div className="form-group">
								<label htmlFor="body" className="text-light">
									Message
								</label>
								<input
									name="body"
									className="form-control bg-light"
									value={this.state.body}
									onChange={this.handleChange}
									autoComplete="off"
								/>
							</div>
							{this.state.error && <p>{this.state.error.message}</p>}
							<button type="submit" className="btn btn-dark mr-3">
								Post
							</button>
							<button
								className="btn btn-danger"
								onClick={this.props.toggleAddPost}
							>
								Cancel
							</button>
						</form>
					</div>
				</div>
			</AddPostWrapper>
		);
	}
}
export default compose(
	withPostContext,
	withToast
)(AddPost);

const AddPostWrapper = styled.div`
	width: 100%;
	height: 100vh;
	position: fixed;
	top: 0;
	left: 0;
	z-index: 100;
	background: rgba(0, 0, 0, 0.75);
	overflow: scroll;

	.form-control {
		color: black;
	}
	::placeholder {
		color: black;
	}
`;
