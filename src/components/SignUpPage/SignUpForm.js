import React, { Component } from "react";
import { compose } from "recompose";
import { withFirebaseContext } from "../../firebase";
import { withRouter } from "react-router-dom";
import { ROUTES } from "../../constants";

const INITIAL_STATE = {
  username: "",
  email: "",
  password: "",
  error: null
};

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...INITIAL_STATE
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    console.log(this.props);
  }

  handleSubmit(e) {
    const { username, email, password } = this.state;
    this.props.firebase
      .doCreateUserWithEmailAndPassword(email, password)
      .then((authUser) => {
        return authUser.user.updateProfile({
          displayName: username
        });
      })
      .then(() => {
        console.log(this.props.firebase.auth.currentUser);
        this.props.firebase.doSignOut();
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch((error) => {
        this.setState({ error });
      });
    e.preventDefault();
  }

  handleChange({ target }) {
    const { name, value } = target;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className='container my-3'>
        <div className='row'>
          <div className='col-10 col-sm-8 mx-auto'>
            <h1 className='lead text-center'>Sign up</h1>
            <hr />
            <form
              className='border border-dark rounded p-3 shadow'
              onSubmit={this.handleSubmit}>
              <div className='form-group'>
                <label htmlFor='username'>Username</label>
                <input
                  type='text'
                  className='form-control'
                  name='username'
                  value={this.state.username}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <div className='form-group'>
                <label htmlFor='email'>Email</label>
                <input
                  type='text'
                  className='form-control'
                  name='email'
                  value={this.state.name}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <div className='form-group'>
                <label htmlFor='password'>Password</label>
                <input
                  type='password'
                  name='password'
                  value={this.state.password}
                  onChange={this.handleChange}
                  className='form-control'
                  autoComplete="off"
                />
              </div>
              {this.state.error && (
                <div className='text-danger'>{this.state.error.message}</div>
              )}
              <button type='submit' className='btn btn-block btn-dark mt-4'>
                Sign up
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  withRouter,
  withFirebaseContext
)(SignUpForm);
