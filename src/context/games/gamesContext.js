import React from 'react';
import { withFirebaseContext } from '../../firebase';
export const GamesContext = React.createContext();
export const GamesContextConsumer = GamesContext.Consumer;

class GamesContextProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			games: [],
			isLoaded: false
		};
		this.getSingleGame = this.getSingleGame.bind(this);
	}

	componentDidMount() {
		this.listener = this.props.firebase.watchCollection('games', games => {
			this.setState({ games, isLoaded: true });
		});
	}

	getSingleGame(id) {
		const game = this.state.games.filter(game => game.id === id);
		return game[0];
	}

	render() {
		return (
			<GamesContext.Provider
				value={{
					...this.state,
					getSingleGame: this.getSingleGame
				}}
			>
				{this.props.children}
			</GamesContext.Provider>
		);
	}
}

export default withFirebaseContext(GamesContextProvider);
