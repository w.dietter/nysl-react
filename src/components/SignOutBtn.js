import React from 'react';
import { withFirebaseContext } from '../firebase';
import { withRouter } from 'react-router-dom';
import { ROUTES } from '../constants';
import { compose } from 'recompose';
import { FaPowerOff } from 'react-icons/fa';
import styled from 'styled-components';

const SignOutBtn = ({ firebase, history }) => {
	const handleSignOut = () => {
		firebase
			.doSignOut()
			.then(() => {
				history.push(ROUTES.LOGIN);
			})
			.catch(error => console.log(error));
	};

	return (
		<SignOutBtnWrapper onClick={handleSignOut}>
			<FaPowerOff /> &nbsp;logout
		</SignOutBtnWrapper>
	);
};

export default compose(
	withRouter,
	withFirebaseContext
)(SignOutBtn);

const SignOutBtnWrapper = styled.div`
	font-size: 1rem;
	padding: 1rem;
`;
