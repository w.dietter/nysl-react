import React, { Component } from "react";
import { withFirebaseContext } from "../../firebase";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { ROUTES } from "../../constants";

const INITIAL_STATE = {
  email: "",
  password: "",
  error: null
};

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange({ target }) {
    const { name, value } = target;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    const { email, password } = this.state;
    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch((error) => {
        this.setState({error});
      })


    e.preventDefault();
  }

  render() {
    return (
      <div className='container mt-5 my-sm-3'>
        <div className='row'>
          <div className='col-10 col-sm-8 mx-auto'>
            <h1 className='lead text-center'>Log in</h1>
            <hr />
            <form
              className='border border-dark rounded p-3 shadow'
              onSubmit={this.handleSubmit}>
              <div className='form-group'>
                <label htmlFor='email'>Email</label>
                <input
                  type='text'
                  className='form-control'
                  name='email'
                  value={this.state.name}
                  onChange={this.handleChange}
                  autoComplete="off"
                />
              </div>
              <div className='form-group'>
                <label htmlFor='password'>Password</label>
                <input
                  type='password'
                  name='password'
                  value={this.state.password}
                  onChange={this.handleChange}
                  className='form-control'
                  autoComplete="off"
                />
              </div>
              {this.state.error && (
                <div className='text-danger'>{this.state.error.message}</div>
              )}
              <button type='submit' className='btn btn-block btn-dark mt-4'>
                Log in
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  withRouter,
  withFirebaseContext
)(LoginForm);
