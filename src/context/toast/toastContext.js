import React from 'react';

export const ToastContext = React.createContext();
export const ToastContextConsumer = ToastContext.Consumer;

class ToastContextProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isToastVisible: false,
			text: 'toast',
			timeout: 4000
		};
		this.showToast = this.showToast.bind(this);
		this.hideToast = this.hideToast.bind(this);
	}

	showToast(text, timeout) {
		console.log('funciona show toast');
		this.setState({ isToastVisible: true, text, timeout });
		setTimeout(() => {
			this.hideToast();
		}, timeout);
	}

	hideToast() {
		this.setState({ isToastVisible: false });
	}

	render() {
		return (
			<ToastContext.Provider
				value={{ ...this.state, showToast: this.showToast }}
			>
				{this.props.children}
			</ToastContext.Provider>
		);
	}
}

export default ToastContextProvider;
