import React from "react";
import "./withLoader.css";

const withLoader = (WrappedComponent) => {
  return ({ isLoaded, ...props }) => {
    return isLoaded ? (
      <WrappedComponent {...props} />
    ) : (
      <div className='Loader'>
        <div />
        <div />
        <div />
      </div>
    );
  };
};

export default withLoader;
