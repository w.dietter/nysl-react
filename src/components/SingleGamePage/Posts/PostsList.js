import React from 'react';
import styled from 'styled-components';
import PostItem from './PostItem';
import withLoader from '../../../hoc/withLoader/withLoader';
import { compose } from 'recompose';
import withPostContext from '../../../hoc/withPostContext/withPostContext';

class PostsList extends React.Component {
	render() {
		const { game } = this.props;
		const { getGamePosts } = this.props.postContext;
		const posts = getGamePosts(game.id);
		return (
			<PostListWrapper>
				{posts &&
					posts.map(post => {
						return (
							<PostItem
								key={post.id}
								post={post}
								postContext={this.props.postContext}
							/>
						);
					})}
			</PostListWrapper>
		);
	}
}

const PostListWrapper = styled.ul`
	width: 100%;
	height: 100%;
	overflow: scroll;
	margin-top: 0.1rem;
	padding: 3rem 0 1rem 0;
`;

export default compose(
	withLoader,
	withPostContext
)(PostsList);
