import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

/* components */
import { MdComment } from 'react-icons/md';

/* hoc */
import withPostContext from '../../../hoc/withPostContext/withPostContext';

class CommentsContainer extends React.Component {
	render() {
		const { postId } = this.props;
		return (
			<CommentsContainerWrapper>
				<button className="commentsBtn" onClick={this.handleShowCollapse}>
					<Link to={`/posts/${postId}`}>
						See comments &nbsp; <MdComment />
					</Link>
				</button>
			</CommentsContainerWrapper>
		);
	}
}

export default withPostContext(CommentsContainer);

const CommentsContainerWrapper = styled.div`
	width: 100%;
	.commentsBtn {
		border: none;
		display: block;
		width: 100%;
	}

	.commentsBtn a {
		color: black;
		display: flex;
		align-items: center;
		justify-content: center;
	}
`;
