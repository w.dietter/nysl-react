export const HOME = '/home';
export const GAME_SCHEDULE = '/game-schedule';
export const LOGIN = '/login';
export const SIGN_UP = '/sign-up';
export const POSTS = '/posts';
export const SINGLE_GAME = '/game/:id';
export const POSTCOMMENTS = '/posts/:postId';
export const LANDING = '/';
