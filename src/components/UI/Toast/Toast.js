import React from 'react';
import styled from 'styled-components';
import withToast from '../../../hoc/withToast/withToast';

const Toast = ({
	toastContext: { isToastVisible, text, timeout },
	bottom,
	otherProps
}) => {
	return isToastVisible ? (
		<ToastWrapper
			isToastVisible={isToastVisible}
			bottom={false}
			timeout={timeout}
		>
			<span>{text}</span>
		</ToastWrapper>
	) : null;
};

export default withToast(Toast);

const ToastWrapper = styled.div`
	position: fixed;
	top: 10rem;
	left: 10%;
	z-index: 2000;
	background: black;
	color: white;
	padding: 1rem 2rem;
	border-radius: 2px;
	border: 5px solid white;
	box-shadow: 1px 1px 10px 1px rgba(100, 100, 100, 0.5);
	animation: slideInLeft 6000ms ease-in-out forwards 1;
	animation-name: ${props => (props.bottom ? 'slideInBottom' : 'slideInLeft')};
	animation-duration: ${props => (props.timeout ? props.timeout : 6000)}ms;
	@keyframes slideInBottom {
		0% {
			transform: translateY(5rem);
			opacity: 0;
		}
		15% {
			transform: translateY(0);
			opacity: 1;
		}
		95% {
			transform: translateY(0);
			opacity: 1;
		}
		100% {
			transform: translateY(5rem);
			opacity: 0;
		}
	}

	@keyframes slideInLeft {
		0% {
			transform: translateX(-5rem);
			opacity: 0;
		}
		15% {
			transform: translateX(0);
			opacity: 1;
		}
		95% {
			transform: translateX(0);
			opacity: 1;
		}
		100% {
			transform: translateX(-5rem);
			opacity: 0;
		}
	}
`;
