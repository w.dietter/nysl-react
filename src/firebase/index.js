import Firebase from "./firebase";
import FirebaseContext, { withFirebaseContext } from "./firebaseContext";

export default Firebase;
export { FirebaseContext, withFirebaseContext };
