import React from 'react';
import styled from 'styled-components';

/* components */
import GameItem from './GameItem';
/* hoc */
import withLoader from '../../hoc/withLoader/withLoader';

const GameContainer = ({ games }) => {
	return (
		<GameContainerWrapper>
			{games.map((game, i, a) => (
				<GameItem key={game.id} game={game} index={i + 1} length={a.length} />
			))}
		</GameContainerWrapper>
	);
};

const GameContainerWrapper = styled.div`
	width: 100vw;
	min-height: var(--main-height);
	overflow: auto;
	display: flex;
	scroll-snap-type: x mandatory;

	::-webkit-scrollbar {
		display: none;
	}

	@media screen and (orientation: landscape) {
		min-height: auto;
	}
`;
export default withLoader(GameContainer);
