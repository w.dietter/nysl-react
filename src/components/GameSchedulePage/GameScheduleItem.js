import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

function GameScheduleItem({ game }) {
	return (
		<GameScheduleItemWrapper>
			<td>
				<Link to={`/game/${game.id}`} className="link">
					<span>{game.teams}</span>
				</Link>
			</td>
			<td>
				<span>{game.location.name}</span>
			</td>
			<td>
				<span>{game.date}</span>
			</td>
			<td>
				<span>{game.time}</span>
			</td>
		</GameScheduleItemWrapper>
	);
}
export default GameScheduleItem;

const GameScheduleItemWrapper = styled.tr`
	td {
		padding: 1.5rem 0.2rem;
		font-size: 0.9rem;
	}
	.link {
		color: black;
		text-decoration: underline;
	}
`;
