import React from 'react';
import { withFirebaseContext } from '../../firebase';
export const PostContext = React.createContext();
export const PostContextConsumer = PostContext.Consumer;

class PostContextProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			posts: [],
			comments: [],
			isLoaded: false
		};
		this.getGamePosts = this.getGamePosts.bind(this);
		this.addGamePost = this.addGamePost.bind(this);
		this.getPostComments = this.getPostComments.bind(this);
		this.addCommentToPost = this.addCommentToPost.bind(this);
		this.deleteComment = this.deleteComment.bind(this);
		this.deleteGamePost = this.deleteGamePost.bind(this)
	}

	componentDidMount() {
		const { firebase } = this.props;
		this.postsListener = firebase.watchCollection('posts', posts => {
			posts.sort((a, b) => {
				return a['date'] < b['date'] ? 1 : -1;
			});
			this.setState({ posts });
			this.commentsListener = firebase.watchCollection('comments', comments => {
				comments.sort((a, b) => {
					return a['date'] < b['date'] ? 1 : -1;
				});
				this.setState({ comments, isLoaded: true });
			});
		});
	}

	addGamePost(data) {
		return this.props.firebase.doAddToCollection('posts', {
			...data,
			date: Date.now()
		});
	}

	deleteGamePost(postId) {
		return this.props.firebase.doDeleteFromCollection('posts', postId);
	}

	getGamePosts(gameId) {
		const gamePosts = this.state.posts.filter(post => post.gameId === gameId);
		return gamePosts;
	}

	addCommentToPost(comment) {
		const newComment = {
			...comment,
			date: Date.now()
		};
		return this.props.firebase.doAddToCollection('comments', newComment);
	}

	getPostComments(postId) {
		const postComments = this.state.comments.filter(
			comment => comment.postId === postId
		);
		return postComments;
	}

	deleteComment(commentId) {
		return this.props.firebase.doDeleteFromCollection('comments', commentId);
	}

	componentWillUnmount() {
		this.postsListener();
		this.commentsListener();
	}

	render() {
		return (
			<PostContext.Provider
				value={{
					...this.state,
					getGamePosts: this.getGamePosts,
					getPostComments: this.getPostComments,
					addCommentToPost: this.addCommentToPost,
					addGamePost: this.addGamePost,
					deleteComment: this.deleteComment,
					deleteGamePost: this.deleteGamePost
				}}
			>
				{this.props.children}
			</PostContext.Provider>
		);
	}
}

export default withFirebaseContext(PostContextProvider);
