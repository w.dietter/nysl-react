import React from 'react';
import styled from 'styled-components';
import withLoader from '../../hoc/withLoader/withLoader';
import PostDashboard from './posts/PostDashboard';
import PostsList from './posts/PostsList';
const GamePosts = ({ authUser, game }) => {
	return (
		<GamePostsWrapper>
			<PostDashboard authUser={authUser} game={game} />
			<PostsList isLoaded={authUser} game={game} />
		</GamePostsWrapper>
	);
};

export default withLoader(GamePosts);

const GamePostsWrapper = styled.div`
	height: 70%;
	overflow: hidden;
	position: relative;
`;
