import React from 'react';

/* components */
import GameContainer from '../components/HomePage/GameContainer';
import Title from '../components/UI/Header/Title';
/* hoc */
import withGamesContext from '../hoc/withGamesContext/withGamesContext';

const HomePage = ({ gamesContext: { games, isLoaded } }) => {
	console.log(games, isLoaded);
	return (
		<>
			<Title title="Today Games" />
			<GameContainer games={games} isLoaded={isLoaded} />
		</>
	);
};

export default withGamesContext(HomePage);
