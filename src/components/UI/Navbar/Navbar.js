import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import * as ROUTES from '../../../constants/routes';

function Navbar() {
	return (
		<NavWrapper className="nav shadow-sm">
			<ul>
				<li>
					<NavLink to={ROUTES.HOME} activeClassName="active">
						Home
					</NavLink>
				</li>
				<li>
					<NavLink to={ROUTES.GAME_SCHEDULE} activeClassName="active">
						Schedule
					</NavLink>
				</li>
			</ul>
		</NavWrapper>
	);
}

const NavWrapper = styled.nav`
	background: var(--headerColor);
	height: var(--navbar-height);
	ul {
		width: 100%;
		height: 100%;
		display: flex;
	}

	ul li {
		border: none;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.7);
		list-style: none;
		display: flex;
		align-items: center;
		justify-content: center;
	}

	ul li a {
		width: 100%;
		height: 100%;
		color: white;
		display: flex;
		align-items: center;
		justify-content: center;
		text-decoration: none;
		text-align: center;
	}

	.active {
		color: #fff;
		background: var(--altBgColor);
		padding: 1rem 2rem;
		border: none;
	}

	@media screen and (orientation: landscape) {
		height: calc(var(--navbar-height) + 1rem);
	}
`;

export default Navbar;
