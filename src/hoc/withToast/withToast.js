import React from 'react';
import { ToastContextConsumer } from '../../context/toast/toastContext';

const withToast = WrappedComponent => props => (
	<ToastContextConsumer>
		{toastContext => (
			<WrappedComponent {...props} toastContext={toastContext} />
		)}
	</ToastContextConsumer>
);

export default withToast;
