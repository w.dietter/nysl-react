import React from 'react';
import styled from 'styled-components';
import withAuthentication from '../../../session';
import SignOutBtn from '../../SignOutBtn';
import { Link } from 'react-router-dom';
import { ROUTES } from '../../../constants';
import { FaUserCircle } from 'react-icons/fa';
import logo from '../../../imgs/nysl_logo.png';

const Header = ({ authUser }) => {
	return (
		<HeaderWrapper>
			<div className="brand">
				<img src={logo} alt="logo" />
				<h1>NYSL</h1>
			</div>
			{authUser ? (
				<div className="user-status">
					<div>{authUser.displayName}</div>
					<SignOutBtn />
				</div>
			) : (
				<div className="user-status">
					<Link to={ROUTES.LOGIN}>
						Login <FaUserCircle className="icon" />
					</Link>
				</div>
			)}
		</HeaderWrapper>
	);
};

export default withAuthentication(Header);

const HeaderWrapper = styled.header`
	height: var(--header-height);
	background: var(--headerColor);
	color: #fff;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0.3rem 1rem;

	.brand {
		width: 30%;
		display: flex;
		align-items: center;
	}

	.user-status {
		width: 70%;
		text-align: right;
		height: 100%;
		padding: 1rem 0;
		display: flex;
		justify-content: flex-end;
		align-items: center;
	}

	.icon {
		margin-left: 1rem;
	}

	img {
		width: 40px;
	}
	h1 {
		font-size: 1.7rem;
		margin-left: 0.5rem;
	}

	a {
		height: 100%;
		color: white;
		display: flex;
		align-items: center;
		justify-content: center;
		text-decoration: none;
		text-align: center;
	}

	@media screen and (orientation: landscape) {
		height: calc(var(--header-height) + 1rem);
		padding: 0 3rem;
	}
`;
