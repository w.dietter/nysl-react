import React from 'react';
import styled from 'styled-components';
import { FaRegTrashAlt, FaUserAlt } from 'react-icons/fa';
import withPostContext from '../../../hoc/withPostContext/withPostContext';
import withToast from '../../../hoc/withToast/withToast';
import { compose } from 'recompose';

const CommentsItem = props => {
	const {
		comment,
		authUser,
		postContext: { deleteComment },
		toastContext: { showToast }
	} = props;
	const { user, body, date } = comment;

	const handleDelete = async () => {
		try {
			await deleteComment(comment.id);
			showToast('Comment deleted!', 3000);
		} catch (error) {
			showToast(error.message, 3000);
		}
	};
	return (
		<CommentsItemWrapper className="shadow-sm">
			{authUser && authUser.uid === user.uid ? (
				<FaRegTrashAlt className="icon" onClick={handleDelete} />
			) : null}
			<div className="user">
				<FaUserAlt /> &nbsp;
				{user.displayName} says...
			</div>
			<p className="body">{body}</p>
			<p className="date">{new Date(date).toLocaleString()}</p>
		</CommentsItemWrapper>
	);
};
export default compose(
	withPostContext,
	withToast
)(CommentsItem);

const CommentsItemWrapper = styled.div`
	width: 100%;
	margin: .75rem auto;
	font-size: 0.9rem;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-bottom: 5px solid rgba(0, 0, 0, 0.2);
	/* border-radius: 5px; */
	position: relative;
	background: rgba(180, 200, 180, 0.9);
	padding: 0 0 0.5rem 0;

	.icon {
		position: absolute;
		top: 5px;
		right: 5px;
	}

	.body {
		padding: 0.5rem;
		margin: 0;
	}

	.date {
		padding: 0.25rem;
		margin: 1rem 0 0 0;
		text-align: right;
	}

	.user {
		width: 100%;
		background: rgba(0,0,0,.25);
		text-align: left;
		padding: 0 0 .2rem 0;
		margin: 0;
		border-bottom: 1px solid black;
	}
`;
