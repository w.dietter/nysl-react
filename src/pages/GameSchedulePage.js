import React from 'react';
import Title from '../components/UI/Header/Title';
import GameSchedule from '../components/GameSchedulePage/GameSchedule';
import withGamesContext from '../hoc/withGamesContext/withGamesContext';

function GameSchedulePage({ gamesContext: { games, isLoaded } }) {
	return (
		<div>
			<Title title="Complete games schedule" />
			<GameSchedule isLoaded={isLoaded} games={games} />
		</div>
	);
}
export default withGamesContext(GameSchedulePage);
