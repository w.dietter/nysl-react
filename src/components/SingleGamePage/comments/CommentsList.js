import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
/* components */
import CommentsForm from './CommentsForm';
import CommentsItem from './CommentsItem';
/* hoc */
import { compose } from 'recompose';
import withLoader from '../../../hoc/withLoader/withLoader';
import withAuthentication from '../../../session';

const CommentsList = ({ authUser, postId, comments }) => {
	return (
		<CommentsListWrapper>
			<CommentsForm authUser={authUser} postId={postId} />
			{comments &&
				comments.map(comment => (
					<CommentsItem
						key={comment.id}
						comment={comment}
						authUser={authUser}
					/>
				))}
		</CommentsListWrapper>
	);
};

CommentsList.propTypes = {
	authUser: PropTypes.object.isRequired,
	postId: PropTypes.string.isRequired,
	comments: PropTypes.array.isRequired
};

export default compose(
	withAuthentication,
	withLoader
)(CommentsList);

const CommentsListWrapper = styled.div`
	overflow: hidden;
`;
