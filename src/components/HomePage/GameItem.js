import React from 'react';
import styled from 'styled-components';
import { FaFutbol } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const GameItem = ({ game, index, length }) => {
	return (
		<GameItemWrapper>
			<Link className="link" to={`/game/${game.id}`}>
				<div className="game-wrapper shadow">
					<div>
						<h4>{game.teams}</h4>
					</div>
					<div>
						<FaFutbol className="icon" />
					</div>
					<div>
						<p>{game.location.name}</p>
						<p>{game.date}</p>
						<p>{game.time}</p>
					</div>
				</div>
				<div className="currentGame">
					{index}/{length}
				</div>
			</Link>
		</GameItemWrapper>
	);
};

export default GameItem;

const GameItemWrapper = styled.div`
	flex-shrink: 0;
	scroll-snap-align: start;
	-webkit-overflow-scrolling: touch;
	scroll-snap-stop: always;
	width: 100vw;
	min-height: 100%;
	border-bottom: 2px solid var(--rgbaBlack);

	.link {
		width: 100%;
		min-height: 100%;
		margin: 0%;
		padding: 0;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		text-decoration: none;
		color: black;
	}

	.game-wrapper {
		width: 90%;
		min-height: 100%;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: space-around;
		padding: 1rem 0;
		border: 3px solid rgba(0, 0, 0, 0.4);
		border-radius: 10px;
	}

	.game-wrapper div {
		width: 100%;
		text-align: center;
		padding: 0.5rem 0;
	}

	.icon {
		font-size: 1.4rem;
	}

	h4 {
		width: 100%;
		position: relative;
		font-size: 2rem;
		font-weight: bold;
		text-transform: uppercase;
		text-align: center;
		letter-spacing: 0.9px;
		padding-bottom: 0.5rem;
		border-bottom: 3px solid rgba(0, 0, 0, 0.4);
	}

	p {
		text-align: center;
		font-size: 1.2rem;
	}

	.currentGame {
		width: 100%;
		padding: 1rem;
		display: flex;
		justify-content: space-evenly;
		align-items: center;
	}

	@media screen and (orientation: landscape) {
		width: 50vw;
		min-height: auto;
		height: 50%;

		.link {
			width: 100%;
			min-height: 50%;
			margin: 1rem 0 0 0;
			padding: 0;
		}

		.game-wrapper {
			width: 70%;
			padding: 0;
		}

		h4 {
			font-size: 1rem;
		}

		p {
			font-size: 1rem;
		}

		.currentGame {
			display: none;
		}
	}
`;
