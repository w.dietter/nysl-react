import React from 'react';
import { AuthUserContext } from '../session';
import { Route, Redirect } from 'react-router-dom';
import { ROUTES } from '../constants';

class GuardRoute extends React.Component {
	render() {
		return (
			<AuthUserContext.Consumer>
				{authUser => {
					return (
						<>
							{authUser ? (
								<Route {...this.props} />
							) : (
								<Redirect to={ROUTES.LOGIN} />
							)}
						</>
					);
				}}
			</AuthUserContext.Consumer>
		);
	}
}

export default GuardRoute;
