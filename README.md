Mobile Web App realizada para NYSL liga juvenil de fútbol.
Permite visualizar los próximos partidos, autenticarse con email y password.
Realizar posteos (solo usuarios loggeados) y comentarios en los posteos.

Tecnologías: 
+ ReactJs
+ Firebase/Firestore
+ styled-components


### `npm start`


https://nysl-react.firebaseapp.com/


