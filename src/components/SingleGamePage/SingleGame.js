import React from 'react';
import styled from 'styled-components';
import { FaMapMarkerAlt } from 'react-icons/fa';

function SingleGame(props) {
	const { game } = props;
	return (
		<SingleGameWrapper>
			<div className="title">
				<h1>{game.teams}</h1>
			</div>
			<div className="location">
				<h2>Location</h2>
				<p>{game.location.name}</p>
				<p>{game.location.address}</p>
				<div className="map-link">
					<p>Click to see the map</p>
					<a href={game.location.link}>
						<FaMapMarkerAlt className="icon" />
					</a>
				</div>
			</div>
		</SingleGameWrapper>
	);
}
export default SingleGame;

const SingleGameWrapper = styled.div`
	min-height: 30%;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	padding: 0.5rem 0 1rem 0;

	.title {
		font-size: 1.5rem;
	}

	.icon {
		font-size: 1.5rem;
		margin-right: 3rem;
	}

	.map-link {
		display: flex;
		justify-content: space-between;
	}
`;
