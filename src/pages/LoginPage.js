import React from 'react';
import { Link } from 'react-router-dom';
import LoginForm from '../components/LoginPage/LoginForm';
import { withFirebaseContext } from '../firebase';
import { ROUTES } from '../constants';
import styled from 'styled-components';

function LoginPage({ firebase }) {
	return (
		<LoginPageWrapper>
			<LoginForm />
			<p>
				Dont have an account? <Link to={ROUTES.SIGN_UP}>Sign Up!</Link>
			</p>
		</LoginPageWrapper>
	);
}
export default withFirebaseContext(LoginPage);

const LoginPageWrapper = styled.div`
	p {
		text-align: center;
		margin: 1rem 0;
	}

	a {
		color: black;
		text-decoration: underline;
	}

	.form-control,
	.form-control:focus {
		background: rgba(0, 0, 0, 0.1);
		border: none !important;
		outline: none;
		border-color: #ff0000;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 2px rgba(0, 0, 0, 0.6);
	}
`;
