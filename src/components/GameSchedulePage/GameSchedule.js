import React from 'react';
import GameScheduleItem from './GameScheduleItem';
import withLoader from '../../hoc/withLoader/withLoader';
import styled from 'styled-components';
import {
	FaTshirt,
	FaLocationArrow,
	FaRegClock,
	FaCalendarAlt
} from 'react-icons/fa';

function GameSchedule({ games }) {
	return (
		<GameScheduleWrapper>
			<table>
				<thead>
					<tr>
						<th>
							<span>Teams</span>
							<span>
								<FaTshirt className="icon" />
							</span>
						</th>
						<th>
							<span>Location</span>
							<span>
								<FaLocationArrow className="icon" />
							</span>
						</th>
						<th>
							<span>Date</span>
							<span>
								<FaCalendarAlt className="icon" />
							</span>
						</th>
						<th>
							<span>Time</span>
							<span>
								<FaRegClock className="icon" />
							</span>
						</th>
					</tr>
				</thead>
				<tbody>
					{games &&
						games.map(game => <GameScheduleItem key={game.id} game={game} />)}
				</tbody>
			</table>
		</GameScheduleWrapper>
	);
}

export default withLoader(GameSchedule);

const GameScheduleWrapper = styled.div`
	height: var(--main-height);
	margin: 0 !important;
	padding: 0;
	width: 100vw;

	table {
		width: 100%;
	}

	.icon {
		margin-left: 0.1rem;
	}

	thead {
		font-size: 0.8rem;
		background: black;
		color: white;
		text-align: center;
		padding: 1rem;
	}

	thead tr th {
		padding: 0.25rem 0;
	}

	thead tr th span {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		padding: 0.5rem 1rem;
		padding: 0.25rem;
		margin: 0;
	}
`;
