import React from 'react';
import AuthUserContext from './authUserContext';
import { withFirebaseContext } from '../firebase';

const withAuthentication = WrappedComponent => {
	class WithAuthentication extends React.Component {
		constructor(props) {
			super(props);
			this.state = {
				authUser: null
			};
		}

		componentDidMount() {
			this.listener = this.props.firebase.auth.onAuthStateChanged(authUser => {
				authUser
					? this.setState({ authUser })
					: this.setState({ authUser: null });
			});
		}

		componentWillUnmount() {
			this.listener();
		}

		render() {
			return (
				<AuthUserContext.Provider value={this.state.authUser}>
					<AuthUserContext.Consumer>
						{value => <WrappedComponent authUser={value} {...this.props} />}
					</AuthUserContext.Consumer>
				</AuthUserContext.Provider>
			);
		}
	}

	return withFirebaseContext(WithAuthentication);
};

export default withAuthentication;
