import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
	apiKey: 'AIzaSyD2SYEKk5YF5SCWfm2rjHoVVGN8zsu6_IA',
	authDomain: 'nysl-react.firebaseapp.com',
	databaseURL: 'https://nysl-react.firebaseio.com',
	projectId: 'nysl-react',
	storageBucket: '',
	messagingSenderId: '678563095556',
	appId: '1:678563095556:web:44866992eb225e26'
};

class Firebase {
	constructor() {
		app.initializeApp(firebaseConfig);
		this.auth = app.auth();
		this.db = app.firestore();
	}

	doCreateUserWithEmailAndPassword = (email, password) => {
		return this.auth.createUserWithEmailAndPassword(email, password);
	};

	doSignInWithEmailAndPassword = (email, password) => {
		return this.auth.signInWithEmailAndPassword(email, password);
	};

	/* Needs the user to be logged in */

	doSignOut = () => this.auth.signOut();

	doPasswordReset = (email) => {
		return this.auth.sendPasswordResetEmail(email);
	};

	doPasswordUpdate = (password) => {
		return this.auth.currentUser.updatePassword(password);
	};

	watchUserChanges = (cb) => {
		const unsuscribe = this.auth.onAuthStateChanged((user) => {
			/* detectes if a user session is active. */
			if (user && !user.isAnonymous) {
				console.log(user);
				cb({
					id: user.uid,
					email: user.email,
					displayNAme: user.displayName
				});
			} else {
				cb(null);
			}
		});
		return unsuscribe;
	};

	/* FireStore */

	watchCollection = (collection, cb) => {
		const unsubscribe = this.db
			.collection(collection)
			.onSnapshot((snapshot) => {
				/* snapshot is not an array, but has the forEach method. */
				const docs = [];
				snapshot.forEach((doc) => {
					const data = doc.data();
					docs.push({
						...data,
						id: doc.id
					});
				});
				cb(docs);
			});
		return unsubscribe;
	};

	doAddToCollection = (collection, data) => {
		return this.db.collection(collection).add(data);
	};

	doDeleteFromCollection = (collection, id) => {
		return this.db.collection(collection)
			.doc(id)
			.delete()
	};
	// Add a new document with a generated id.
}

export default Firebase;
