import React from 'react';
import withPostContext from '../hoc/withPostContext/withPostContext';
import CommentsList from '../components/SingleGamePage/comments/CommentsList';
import withAuthentication from '../session';
import { compose } from 'recompose';

const PostCommentsPage = ({
	match,
	postContext: { getPostComments },
	authUser
}) => {
	const { postId } = match.params;
	const postComments = getPostComments(postId);
	return (
		<CommentsList
			isLoaded={postComments !== [] && authUser}
			comments={postComments}
			postId={postId}
			authUser={authUser}
		/>
	);
};

export default compose(
	withAuthentication,
	withPostContext
)(PostCommentsPage);
