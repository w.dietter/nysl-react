import React from 'react';
import { PostContextProvider, PostContextConsumer } from '../../context';

const withPostContext = WrappedComponent => {
	return props => {
		return (
			<PostContextProvider>
				<PostContextConsumer>
					{postContext => (
						<WrappedComponent {...props} postContext={postContext} />
					)}
				</PostContextConsumer>
			</PostContextProvider>
		);
	};
};

export default withPostContext;
