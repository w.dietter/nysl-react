import React from 'react';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import GameSchedulePage from './pages/GameSchedulePage';
import { Navbar, Header } from './components/UI';
import LoginPage from './pages/LoginPage';
import { ROUTES } from './constants';
import SignUpPage from './pages/SignUpPage';
import withAuthentication from './session/withAuthentication';
import SingleGamePage from './pages/SingleGamePage';
import PostCommentsPage from './pages/PostCommentsPage';
import LandingPage from './pages/LandingPage';
import Toast from './components/UI/Toast/Toast';

function App() {
	return (
		<AppWrapper>
			<Header />
			<Navbar />
			<Toast bottom />
			<Switch>
				<Route exact path={ROUTES.LANDING} component={LandingPage} />
				<Route exact path={ROUTES.HOME} component={HomePage} />
				<Route exact path={ROUTES.GAME_SCHEDULE} component={GameSchedulePage} />
				<Route exact path={ROUTES.LOGIN} component={LoginPage} />
				<Route exact path={ROUTES.SIGN_UP} component={SignUpPage} />
				<Route exact path={ROUTES.SINGLE_GAME} component={SingleGamePage} />
				<Route exact path={ROUTES.POSTCOMMENTS} component={PostCommentsPage} />
			</Switch>
		</AppWrapper>
	);
}

const AppWrapper = styled.div`
	height: 100vh;
`;

export default withAuthentication(App);
