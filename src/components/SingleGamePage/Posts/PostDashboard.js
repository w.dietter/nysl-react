import React from 'react';
import styled from 'styled-components';
import { FaPlus } from 'react-icons/fa';
import AddPost from './AddPost';

class PostDashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showAddPost: false
		};
		this.toggleAddPost = this.toggleAddPost.bind(this);
	}

	toggleAddPost() {
		this.setState({ showAddPost: !this.state.showAddPost });
	}

	render() {
		const { authUser, game } = this.props;
		return (
			<>
				<PostDashboardWrapper>
					{/* <h1>Game Posts</h1> */}
					<div>Add a new Post</div>
					<FaPlus
						className="plus-icon"
						onClick={this.toggleAddPost}
					/>
				</PostDashboardWrapper>
				{this.state.showAddPost && (
					<AddPost
						toggleAddPost={this.toggleAddPost}
						authUser={authUser}
						game={game}
					/>
				)}
			</>
		);
	}
}

export default PostDashboard;

const PostDashboardWrapper = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	z-index: 100;
	width: 100%;
	display: flex;
	justify-content: space-between;
	background: black;
	color: white;
	padding: 0.5rem 2rem;

	.plus-icon {
		font-size: 1.5rem;
	}
`;
