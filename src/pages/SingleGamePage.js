import React from 'react';
import styled from 'styled-components';
/* components */
import SingleGame from '../components/SingleGamePage/SingleGame';
import GamePosts from '../components/SingleGamePage/GamePosts';
/* hoc */
import withAuthentication from '../session';
import withGamesContext from '../hoc/withGamesContext/withGamesContext';
import { compose } from 'recompose';
import withLoader from '../hoc/withLoader/withLoader';

function SingleGamePage({ authUser, gamesContext: { games }, match }) {
	const game = games.find(game => game.id === match.params.id);
	return (
		<SingleGamePageWrapper isLoaded={game}>
			<SingleGame game={game} />

			{!authUser && (
				<div className="text-center mt-5 text-uppercase">
					Login to see comments
				</div>
			)}

			{authUser && (
				<GamePosts
					authUser={authUser}
					game={game}
					isLoaded={game && authUser}
				/>
			)}
		</SingleGamePageWrapper>
	);
}
export default compose(
	withAuthentication,
	withGamesContext
)(SingleGamePage);

const SingleGamePageWrapper = withLoader(styled.div`
	width: 100%;
	min-height: 100vh;
	padding-top: 1rem;
`);
