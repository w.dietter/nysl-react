import React from 'react';
import ReactDOM from 'react-dom';
import Firebase, { FirebaseContext } from './firebase';
import App from './App';
import './index.css';
import { BrowserRouter as Router } from 'react-router-dom';
import ToastContextProvider from './context/toast/toastContext';

ReactDOM.render(
	<FirebaseContext.Provider value={new Firebase()}>
		<Router>
			<ToastContextProvider>
				<App />
			</ToastContextProvider>
		</Router>
	</FirebaseContext.Provider>,
	document.getElementById('root')
);
