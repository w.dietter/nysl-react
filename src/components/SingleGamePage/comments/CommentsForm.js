import React from 'react';
import styled from 'styled-components';
import withPostContext from '../../../hoc/withPostContext/withPostContext';
import { compose } from 'recompose';
import withToast from '../../../hoc/withToast/withToast';

class CommentsForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			body: '',
			error: null
		};
		console.log(this.props);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange({ target }) {
		const { name, value } = target;
		this.setState({
			[name]: value
		});
	}

	async handleSubmit(e) {
		e.preventDefault();
		const { body } = this.state;
		const {
			authUser: { email, displayName, uid },
			postId,
			postContext: { addCommentToPost },
			toastContext: { showToast }
		} = this.props;
		console.log(showToast);
		if (this.state.body !== '') {
			try {
				addCommentToPost({
					postId,
					user: {
						email,
						displayName,
						uid
					},
					body
				});
				showToast('New Comment created!', 3000);
				this.setState({ body: '', error: null });
			} catch (error) {
				this.setState({ error });
			}
		} else {
			showToast('Empty messages are not allowed!', 3000);
		}
	}

	render() {
		return (
			<CommentsFormWrapper>
				<form onSubmit={this.handleSubmit}>
					{this.state.error && <div>{this.state.error.message}</div>}
					<div className="input-group">
						<input
							type="text"
							name="body"
							className="form-control"
							value={this.state.body}
							onChange={this.handleChange}
							autoComplete="off"
							placeholder="post a comment..."
						/>

						<div className="input-group-append">
							<button type="submit" className="btn">
								Post
							</button>
						</div>
					</div>
				</form>
			</CommentsFormWrapper>
		);
	}
}
export default compose(
	withPostContext,
	withToast
)(CommentsForm);

const CommentsFormWrapper = styled.div`
	margin-top: 1rem;
	.btn {
		background: black !important;
		color: white !important;
		border: 1px solid black;
		border-radius: 0;
	}

	.form-control {
		border: none;
		border-radius: 0px !important;
	}
	.form-control::placeholder {
		color: black;
	}
`;
