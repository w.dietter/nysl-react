import React from 'react';
import { GamesContextProvider, GamesContextConsumer } from '../../context';

const withGamesContext = WrappedComponent => {
	return props => {
		return (
			<GamesContextProvider>
				<GamesContextConsumer>
					{gamesContext => (
						<WrappedComponent {...props} gamesContext={gamesContext} />
					)}
				</GamesContextConsumer>
			</GamesContextProvider>
		);
	};
};

export default withGamesContext;
